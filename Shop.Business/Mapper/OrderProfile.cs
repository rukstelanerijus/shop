﻿using AutoMapper;

namespace Shop.Business.Mapper
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Shop.Business.Models.Order, Shop.DataAccess.Entities.Order>();
            CreateMap<Shop.DataAccess.Entities.Order, Shop.Business.Models.Order>();
        }
    }
}
