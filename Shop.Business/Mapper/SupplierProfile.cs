﻿using AutoMapper;

namespace Shop.Business.Mapper
{
    public class SupplierProfile : Profile
    {
        public SupplierProfile()
        {
            CreateMap<Shop.Business.Models.Supplier, Shop.DataAccess.Entities.Supplier>();
            CreateMap<Shop.DataAccess.Entities.Supplier, Shop.Business.Models.Supplier>();
        }
    }
}
