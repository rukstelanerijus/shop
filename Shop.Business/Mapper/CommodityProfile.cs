﻿using AutoMapper;

namespace Shop.Business.Mapper
{
    public class CommodityProfile : Profile
    {
        public CommodityProfile()
        {
            CreateMap<Shop.Business.Models.Commodity, Shop.DataAccess.Entities.Commodity>();
            CreateMap<Shop.DataAccess.Entities.Commodity, Shop.Business.Models.Commodity>();
        }
    }
}
