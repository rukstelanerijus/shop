﻿using AutoMapper;
using Shop.Business.Models;
using Shop.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Business.Services
{
    public class CommodityService : ICommodityService
    {
        private readonly IRepository<Shop.DataAccess.Entities.Commodity> repository;
        private readonly ICommodityRepository commodityRepository;
        private readonly IMapper mapper;
        public CommodityService(ICommodityRepository commodityRepository,
                                IMapper mapper,
                                IRepository<Shop.DataAccess.Entities.Commodity> repository)
        {
            this.commodityRepository = commodityRepository;
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task DeleteCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException(nameof(commodity));
            }

            var commodityDao = mapper.Map<Shop.DataAccess.Entities.Commodity>(commodity);
            await repository.Delete(commodityDao);

        }

        public async Task<Commodity> GetCommodityById(int id)
        {
            var commodityDao = await repository.GetById(id);
            return mapper.Map<Shop.Business.Models.Commodity>(commodityDao);
        }

        public async Task<IEnumerable<Commodity>> GetCommodities()
        {
            var commodities = await repository.Get();
            return mapper.Map<IEnumerable<Shop.Business.Models.Commodity>>(commodities);
        }
        public async Task<IEnumerable<Commodity>> GetCommoditiesByName(string name)
        {
            var commodities = await commodityRepository.GetByName(name);
            return mapper.Map<IEnumerable<Shop.Business.Models.Commodity>>(commodities);
        }

        public async Task InsertCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException(nameof(commodity));
            }

            var commodityDao = mapper.Map<Shop.DataAccess.Entities.Commodity>(commodity);
            await repository.Insert(commodityDao);
        }

        public async Task UpdateCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException(nameof(commodity));
            }

            var commodityDao = mapper.Map<Shop.DataAccess.Entities.Commodity>(commodity);
            await repository.Update(commodityDao);
        }
    }
}
