﻿using Shop.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Business.Services
{
    public interface ISupplierService
    {
        Task<IEnumerable<Supplier>> GetSuppliers();
        Task<Supplier> GetSupplierById(int id);
        Task InsertSupplier(Supplier supplier);
        Task UpdateSupplier(Supplier supplier);
        Task DeleteSupplier(Supplier supplier);
    }
}
