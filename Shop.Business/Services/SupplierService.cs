﻿using AutoMapper;
using Shop.Business.Models;
using Shop.Business.Services;
using Shop.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Services.Suppliers
{
    public class SupplierService : ISupplierService
    {
        private readonly IRepository<Shop.DataAccess.Entities.Supplier> supplierRepository;
        private readonly IMapper mapper;

        public SupplierService(IRepository<Shop.DataAccess.Entities.Supplier> supplierRepository, IMapper mapper)
        {
            this.supplierRepository = supplierRepository;
            this.mapper = mapper;
        }

        public async Task DeleteSupplier(Supplier supplier)
        {
            if (supplier == null)
            {
                throw new ArgumentNullException(nameof(supplier));
            }
           
            var supplierDao = mapper.Map<Shop.DataAccess.Entities.Supplier>(supplier);
            await supplierRepository.Delete(supplierDao);
           
        }

        public async Task<Supplier> GetSupplierById(int id)
        {
            var supplierDao = await supplierRepository.GetById(id);
            return mapper.Map<Shop.Business.Models.Supplier>(supplierDao);
        }

        public async Task<IEnumerable<Supplier>> GetSuppliers()
        {
            var suppliers = await supplierRepository.Get();
            return mapper.Map<IEnumerable<Shop.Business.Models.Supplier>>(suppliers);
        }

        public async Task InsertSupplier(Supplier supplier)
        {
            if (supplier == null)
            {
                throw new ArgumentNullException(nameof(supplier));
            }
           
            var supplierDao = mapper.Map<Shop.DataAccess.Entities.Supplier>(supplier);
            await supplierRepository.Insert(supplierDao);
        }

        public async Task UpdateSupplier(Supplier supplier)
        {
            if (supplier == null)
            {
                throw new ArgumentNullException(nameof(supplier));
            }

            var supplierDao = mapper.Map<Shop.DataAccess.Entities.Supplier>(supplier);
            await supplierRepository.Update(supplierDao);
        }
    }
}

