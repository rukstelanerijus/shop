﻿using Shop.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Business.Services
{
    public interface ICommodityService
    {
        Task<IEnumerable<Commodity>> GetCommodities();
        Task<IEnumerable<Commodity>> GetCommoditiesByName(string name);
        Task<Commodity> GetCommodityById(int id);
        Task InsertCommodity(Commodity commodity);
        Task UpdateCommodity(Commodity commodity);
        Task DeleteCommodity(Commodity commodity);
    }
}
