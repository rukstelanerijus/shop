﻿using AutoMapper;
using Shop.Business.Models;
using Shop.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shop.Business.Services
{
    public class OrderService : IOrderService
    {
        private readonly object orderLock = new object();
        private readonly IRepository<Shop.DataAccess.Entities.Order> orderRepository;
        private readonly ICommodityRepository commodityRepository;
        private readonly IMapper mapper;

        public OrderService(IRepository<Shop.DataAccess.Entities.Order> orderRepository,
                            IMapper mapper,
                            ICommodityRepository commodityRepository)
        {
            this.orderRepository = orderRepository;
            this.commodityRepository = commodityRepository;
            this.mapper = mapper;
        }

        public async Task DeleteOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }

            var orderDao = mapper.Map<Shop.DataAccess.Entities.Order>(order);
            await orderRepository.Delete(orderDao);

        }

        public async Task<Order> GetOrderById(int id)
        {
            var orderDao = await orderRepository.GetById(id);
            return mapper.Map<Shop.Business.Models.Order>(orderDao);
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            var orders = await orderRepository.Get();
            return mapper.Map<IEnumerable<Shop.Business.Models.Order>>(orders);
        }

        public async Task InsertOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }

            var orderDao = mapper.Map<Shop.DataAccess.Entities.Order>(order);

            try
            {
                Monitor.Enter(orderLock);
                var result = await CheckIsCommoditiesAvailable(orderDao.Commodities);
                await orderRepository.Insert(orderDao);
            }
            finally
            {
                Monitor.Exit(orderLock);
            }
        }

        public async Task UpdateOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }

            var orderDao = mapper.Map<Shop.DataAccess.Entities.Order>(order);
            await orderRepository.Update(orderDao);
        }

        private async Task<bool> CheckIsCommoditiesAvailable(IEnumerable<DataAccess.Entities.Commodity> commodities)
        {
            foreach (var item in commodities)
            {
                List<Shop.DataAccess.Entities.Commodity> existingCommodities = (await commodityRepository.GetByName(item.Name)).ToList();

                if (existingCommodities.Count == 0)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}
