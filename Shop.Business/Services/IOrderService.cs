﻿using Shop.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Business.Services
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> GetOrders();
        Task<Order> GetOrderById(int id);
        Task InsertOrder(Order order);
        Task UpdateOrder(Order order);
        Task DeleteOrder(Order order);
    }
}
