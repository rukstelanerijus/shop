﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.API.Models.Order;
using Shop.Business.Models;
using Shop.Business.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.API.Controller
{
    [ApiController]
    [Route("api/orders")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;
        private readonly IMapper mapper;

        public OrderController(IOrderService orderService, IMapper mapper)
        {
            this.orderService = orderService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<OrderResponse>> GetOrders()
        {
            var order = await orderService.GetOrders();
            var orderResponse = mapper.Map<IEnumerable<OrderResponse>>(order);
            return orderResponse;
        }

        [HttpPost]
        public async Task<IActionResult> InsertOrder([FromBody] InsertOrderRequest order)
        {
            if (order == null)
            {
                return BadRequest();
            }

            var orderDto = mapper.Map<Order>(order);
            await orderService.InsertOrder(orderDto);
            return CreatedAtAction(nameof(GetOrderById), order);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<OrderResponse>> GetOrderById([FromRoute] int id)
        {
            var Order = await orderService.GetOrderById(id);

            if (Order == null)
            {
                return NotFound();
            }

            var orderResponse = mapper.Map<OrderResponse>(Order);
            return orderResponse;
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteOrder([FromBody] DeleteOrderRequest order)
        {
            if (order == null || order.Id <= 0)
            {
                return BadRequest();
            }

            var orderDto = mapper.Map<Order>(order);
            await orderService.DeleteOrder(orderDto);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateOrder([FromBody] UpdateOrderRequest order)
        {
            if (order == null || order.Id <= 0)
            {
                return BadRequest();
            }

            var orderDto = mapper.Map<Order>(order);
            await orderService.UpdateOrder(orderDto);
            return NoContent();
        }

    }
}
