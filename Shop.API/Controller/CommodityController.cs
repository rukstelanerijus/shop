﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.API.Models.Commodity;
using Shop.Business.Models;
using Shop.Business.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.API.Controller
{
    [ApiController]
    [Route("api/commodities")]
    public class CommodityController : ControllerBase
    {
        private readonly ICommodityService commodityService;
        private readonly IMapper mapper;

        public CommodityController(ICommodityService commodityService, IMapper mapper)
        {
            this.commodityService = commodityService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<CommodityResponse>> GetCommodities()
        {
            var commodity = await commodityService.GetCommodities();
            var commodityResponse = mapper.Map<IEnumerable<CommodityResponse>>(commodity);
            return commodityResponse;
        }

        [HttpGet("{name}")]
        public async Task<IEnumerable<CommodityResponse>> GetCommoditiesByName([FromRoute] string name)
        {
            var commodities = await commodityService.GetCommoditiesByName(name);
            var commodityResponse = mapper.Map<IEnumerable<CommodityResponse>>(commodities);
            return commodityResponse;
        }

        [HttpPost]
        public async Task<IActionResult> InsertCommodity([FromBody] InsertCommodityRequest commodity)
        {
            if (commodity == null)
            {
                return BadRequest();
            }

            var commodityDto = mapper.Map<Commodity>(commodity);
            await commodityService.InsertCommodity(commodityDto);
            return CreatedAtAction(nameof(GetCommodityById), commodity);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<CommodityResponse>> GetCommodityById([FromRoute] int id)
        {
            var commodity = await commodityService.GetCommodityById(id);

            if (commodity == null)
            {
                return NotFound();
            }

            var commodityResponse = mapper.Map<CommodityResponse>(commodity);
            return commodityResponse;
        }

        [HttpDelete]
        public async Task<ActionResult> Deletecommodity([FromBody] DeleteCommodityRequest commodity)
        {
            if (commodity == null || commodity.Id <= 0)
            {
                return BadRequest();
            }

            var commodityDto = mapper.Map<Commodity>(commodity);
            await commodityService.DeleteCommodity(commodityDto);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> Updatecommodity([FromBody] UpdateCommodityRequest commodity)
        {
            if (commodity == null || commodity.Id <= 0)
            {
                return BadRequest();
            }

            var commodityDto = mapper.Map<Commodity>(commodity);
            await commodityService.UpdateCommodity(commodityDto);
            return NoContent();
        }

    }
}
