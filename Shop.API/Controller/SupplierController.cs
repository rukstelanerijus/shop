﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.API.Models.Supplier;
using Shop.Business.Models;
using Shop.Business.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.API.Controller
{
    [ApiController]
    [Route("api/suppliers")]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService supplierService;
        private readonly IMapper mapper;

        public SupplierController(ISupplierService supplierService, IMapper mapper)
        {
            this.supplierService = supplierService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<SupplierResponse>> GetSuppliers()
        {
            var supplier = await supplierService.GetSuppliers();
            var supplierResponse = mapper.Map<IEnumerable<SupplierResponse>>(supplier);
            return supplierResponse;
        }

        [HttpPost]
        public async Task<IActionResult> InsertSupplier([FromBody] InsertSupplierRequest supplier)
        {
            if (supplier == null)
            {
                return BadRequest();
            }

            var supplierDto = mapper.Map<Supplier>(supplier);
            await supplierService.InsertSupplier(supplierDto);
            return CreatedAtAction(nameof(GetSupplierById), supplier);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<SupplierResponse>> GetSupplierById([FromRoute] int id)
        {
            var supplier = await supplierService.GetSupplierById(id);

            if (supplier == null)
            {
                return NotFound();
            }

            var supplierResponse = mapper.Map<SupplierResponse>(supplier);
            return supplierResponse;
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteSupplier([FromBody] DeleteSupplierRequest supplier)
        {
            if (supplier == null || supplier.Id <= 0)
            {
                return BadRequest();
            }

            var supplierDto = mapper.Map<Supplier>(supplier);
            await supplierService.DeleteSupplier(supplierDto);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateSupplier([FromBody] UpdateSupplierRequest supplier)
        {
            if (supplier == null || supplier.Id <= 0)
            {
                return BadRequest();
            }

            var supplierDto = mapper.Map<Supplier>(supplier);
            await supplierService.UpdateSupplier(supplierDto);
            return NoContent();
        }

    }
}
