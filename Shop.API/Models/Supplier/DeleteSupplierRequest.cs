﻿namespace Shop.API.Models.Supplier
{
    public class DeleteSupplierRequest
    {
        public int Id { get; set; }
    }
}
