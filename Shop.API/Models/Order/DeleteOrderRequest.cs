﻿namespace Shop.API.Models.Order
{
    public class DeleteOrderRequest
    {
        public int Id { get; set; }
    }
}
