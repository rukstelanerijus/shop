﻿namespace Shop.API.Models.Commodity
{
    public class DeleteCommodityRequest
    {
        public int Id { get; set; }
    }
}
