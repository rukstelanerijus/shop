﻿using AutoMapper;
using Shop.API.Models.Supplier;
using Shop.Business.Models;

namespace Shop.API.Mapper
{
    public class SupplierProfile : Profile
    {
        public SupplierProfile()
        {
            CreateMap<UpdateSupplierRequest, Supplier>();
            CreateMap<InsertSupplierRequest, Supplier>();
            CreateMap<DeleteSupplierRequest, Supplier>();
            CreateMap<Supplier, SupplierResponse>();
        }
    }
}
