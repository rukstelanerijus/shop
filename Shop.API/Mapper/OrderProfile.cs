﻿using AutoMapper;
using Shop.API.Models.Order;
using Shop.Business.Models;

namespace Shop.API.Mapper
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<UpdateOrderRequest, Order>();
            CreateMap<InsertOrderRequest, Order>();
            CreateMap<DeleteOrderRequest, Order>();
            CreateMap<Order, OrderResponse>();
        }
    }
}
