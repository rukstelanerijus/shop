﻿using AutoMapper;
using Shop.API.Models.Commodity;
using Shop.Business.Models;

namespace Shop.API.Mapper
{
    public class CommodityProfile : Profile
    {
        public CommodityProfile()
        {
            CreateMap<UpdateCommodityRequest, Commodity>();
            CreateMap<InsertCommodityRequest, Commodity>();
            CreateMap<DeleteCommodityRequest, Commodity>();
            CreateMap<Commodity, CommodityResponse>();
        }
    }
}
