﻿using Shop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.DataAccess.Repositories
{
    public interface ICommodityRepository
    {
        Task<IEnumerable<Commodity>> GetByName(string name);
    }
}
