﻿using Shop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.DataAccess.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> Get();

        Task<T> GetById(int id);

        Task Update(T item);

        Task Insert(T item);

        Task Delete(T item);
    }
}
