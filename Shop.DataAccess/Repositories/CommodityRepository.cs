﻿using Microsoft.EntityFrameworkCore;
using Shop.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.DataAccess.Repositories
{
    public class CommodityRepository : ICommodityRepository
    {
        private readonly ShopContext context;

        public CommodityRepository(ShopContext shopContext)
        {
            this.context = shopContext;
        }

        public async Task<IEnumerable<Commodity>> GetByName(string name)
        {
            return await context.Commodities.Where(x=>x.Name == name).ToListAsync();
        }
    }
}