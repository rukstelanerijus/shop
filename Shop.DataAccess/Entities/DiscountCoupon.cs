﻿namespace Shop.DataAccess.Entities
{
    public class DiscountCoupon : BaseEntity
    {
        public decimal Amount { get; set; }
    }
}
