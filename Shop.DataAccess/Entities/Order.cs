﻿using System.Collections.Generic;

namespace Shop.DataAccess.Entities
{
    public class Order : BaseEntity
    {
        public IEnumerable<Commodity> Commodities { get; set; }
        public Client Client { get; set; }
    }
}
