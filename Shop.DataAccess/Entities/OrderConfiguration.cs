﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shop.DataAccess.Entities
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Client).WithMany().HasForeignKey(x => x.Id).OnDelete(DeleteBehavior.Cascade).IsRequired();
            builder.HasMany(x => x.Commodities).WithOne().HasForeignKey(x => x.Id).OnDelete(DeleteBehavior.Cascade).IsRequired();
        }
    }
}
