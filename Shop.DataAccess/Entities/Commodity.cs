﻿using System.Collections.Generic;

namespace Shop.DataAccess.Entities
{
    public class Commodity : BaseEntity
    {
        public string Name { get; set; }
        public Price Price { get; set; }
        public int PriceId { get; set; }
        public IEnumerable<SuppliersCommodities> SuppliersCommodities { get; set; }
    }
}
