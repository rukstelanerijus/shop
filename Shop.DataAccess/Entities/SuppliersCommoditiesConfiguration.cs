﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shop.DataAccess.Entities
{
    public class SuppliersCommoditiesConfiguration : IEntityTypeConfiguration<SuppliersCommodities>
    {
        public void Configure(EntityTypeBuilder<SuppliersCommodities> builder)
        {
            builder.ToTable("SuppliersCommodities");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Supplier).WithMany(x => x.SuppliersCommodities).OnDelete(DeleteBehavior.Cascade).IsRequired();
            builder.HasOne(x => x.Commodity).WithMany(x => x.SuppliersCommodities).OnDelete(DeleteBehavior.Cascade).IsRequired();
        }
    }
}
