﻿namespace Shop.DataAccess.Entities
{
    public class SuppliersCommodities : BaseEntity
    {
        public Supplier Supplier { get; set; }
        public int SupplierId { get; set; }
        public Commodity Commodity { get; set; }
        public int CommodityId { get; set; }
    }
}
