﻿using System.Collections.Generic;

namespace Shop.DataAccess.Entities
{
    public class Supplier : BaseEntity
    {
        public string Name { get; set; }
        public IEnumerable<SuppliersCommodities> SuppliersCommodities { get; set; }
    }
}
