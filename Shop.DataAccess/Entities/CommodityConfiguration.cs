﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shop.DataAccess.Entities
{
    public class CommodityConfiguration : IEntityTypeConfiguration<Commodity>
    {
        public void Configure(EntityTypeBuilder<Commodity> builder)
        {
            builder.ToTable("Commodities");
            builder.Property(x => x.Name).IsRequired().HasMaxLength(512);

            builder.HasOne(x => x.Price).WithOne(x => x.Commodity).HasForeignKey<Commodity>(x => x.PriceId).IsRequired();
        }
    }
}
