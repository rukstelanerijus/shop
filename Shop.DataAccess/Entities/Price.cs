﻿namespace Shop.DataAccess.Entities
{
    public class Price : BaseEntity
    {
        public decimal Amount { get; set; }
        public Commodity Commodity { get; set; }
    }
}
